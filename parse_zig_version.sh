#!/bin/sh

sed '/registry.gitlab.com\/sbenv\/veroxis\/images\/zig/!d' version_manifest.txt | tail -n 1 | cut -d' ' -f5 | cut -d'=' -f2
