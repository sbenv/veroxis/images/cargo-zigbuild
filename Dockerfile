FROM registry.gitlab.com/sbenv/veroxis/images/alpine:3.20.2

COPY "cargo-zigbuild-bin" "/usr/local/bin/cargo-zigbuild"
