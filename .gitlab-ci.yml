zigbuild:build:x86_64:
  image: registry.gitlab.com/sbenv/veroxis/images/rust:1.80.1
  tags: ["_docker", "x86_64"]
  interruptible: true
  artifacts:
    paths:
      - "cargo-zigbuild/target/x86_64-unknown-linux-musl/release/cargo-zigbuild"
  script:
    - set -euo pipefail
    - export ZIGBUILD_VERSION=$(./parse_zigbuild_version.sh)
    - export ZIG_ARCHITECTURE=x86_64
    - export ZIG_VERSION=$(./parse_zig_version.sh)
    - |
      echo "ZIG_ARCHITECTURE: ${ZIG_ARCHITECTURE}"
      echo "ZIG_VERSION: ${ZIG_VERSION}"
      echo "ZIGBUILD_VERSION: ${ZIGBUILD_VERSION}"
    - set -x
    # install required deps
    - apk add "git" "build-base" "tar" "xz" "curl" "jq"
    # install zig
    - curl -s --fail -o "${ZIG_VERSION}.tar.xz" "https://ziglang.org/download/${ZIG_VERSION}/zig-linux-${ZIG_ARCHITECTURE}-${ZIG_VERSION}.tar.xz"
    - tar -xvf "${ZIG_VERSION}.tar.xz"
    - mv "zig-linux-${ZIG_ARCHITECTURE}-${ZIG_VERSION}" "zig"
    - mv "zig/zig" "/usr/bin/zig"
    - mv "zig/lib" "/usr/lib/zig"
    # add required rust toolchains
    - rustup toolchain add "nightly"
    - rustup component add "rust-src" --toolchain="nightly"
    - rustup target add "x86_64-unknown-linux-musl" --toolchain="nightly"
    # clone cargo-zigbuild
    - git clone --branch="v${ZIGBUILD_VERSION}" "https://github.com/messense/cargo-zigbuild.git" "cargo-zigbuild"
    # create a local cargo-zigbuild binary for cross-compilation
    - cargo build --manifest-path="cargo-zigbuild/Cargo.toml" --target="x86_64-unknown-linux-musl"
    - mv cargo-zigbuild/target/x86_64-unknown-linux-musl/debug/cargo-zigbuild /usr/local/bin/cargo-zigbuild
    # build final binary
    - export RUSTFLAGS="-C opt-level=3 -C strip=symbols -C panic=abort -C lto=true -C embed-bitcode=yes -C codegen-units=1"
    - cargo +nightly zigbuild --manifest-path="cargo-zigbuild/Cargo.toml" -Z build-std=std,panic_abort -Z build-std-features=panic_immediate_abort --target="x86_64-unknown-linux-musl" --release

zigbuild:build:aarch64:
  image: registry.gitlab.com/sbenv/veroxis/images/rust:1.80.1
  tags: ["_docker", "x86_64"]
  interruptible: true
  variables:
    RUSTFLAGS: -C opt-level=3 -C strip=symbols -C panic=abort -C link-arg=-static -C target-feature=+crt-static
  artifacts:
    paths:
      - "cargo-zigbuild/target/aarch64-unknown-linux-musl/release/cargo-zigbuild"
  script:
    - set -euo pipefail
    - export ZIGBUILD_VERSION=$(./parse_zigbuild_version.sh)
    - export ZIG_ARCHITECTURE=x86_64
    - export ZIG_VERSION=$(./parse_zig_version.sh)
    - |
      echo "ZIG_ARCHITECTURE: ${ZIG_ARCHITECTURE}"
      echo "ZIG_VERSION: ${ZIG_VERSION}"
      echo "ZIGBUILD_VERSION: ${ZIGBUILD_VERSION}"
    - set -x
    # install required deps
    - apk add "git" "build-base" "tar" "xz" "curl" "jq"
    # install zig
    - curl -s --fail -o "${ZIG_VERSION}.tar.xz" "https://ziglang.org/download/${ZIG_VERSION}/zig-linux-${ZIG_ARCHITECTURE}-${ZIG_VERSION}.tar.xz"
    - tar -xvf "${ZIG_VERSION}.tar.xz"
    - mv "zig-linux-${ZIG_ARCHITECTURE}-${ZIG_VERSION}" "zig"
    - mv "zig/zig" "/usr/bin/zig"
    - mv "zig/lib" "/usr/lib/zig"
    # add required rust toolchains
    - rustup toolchain add "nightly"
    - rustup component add "rust-src" --toolchain="nightly"
    - rustup target add "x86_64-unknown-linux-musl" --toolchain="nightly"
    - rustup target add "aarch64-unknown-linux-musl" --toolchain="nightly"
    # clone cargo-zigbuild
    - git clone --branch="v${ZIGBUILD_VERSION}" "https://github.com/messense/cargo-zigbuild.git" "cargo-zigbuild"
    # create a local cargo-zigbuild binary for cross-compilation
    - cargo build --manifest-path="cargo-zigbuild/Cargo.toml" --target="x86_64-unknown-linux-musl"
    - mv cargo-zigbuild/target/x86_64-unknown-linux-musl/debug/cargo-zigbuild /usr/local/bin/cargo-zigbuild
    # build final binary
    - export RUSTFLAGS="-C opt-level=3 -C strip=symbols -C panic=abort -C lto=true -C embed-bitcode=yes -C codegen-units=1"
    - cargo +nightly zigbuild --manifest-path="cargo-zigbuild/Cargo.toml" -Z build-std=std,panic_abort -Z build-std-features=panic_immediate_abort --target="aarch64-unknown-linux-musl" --release

image:build:x86_64:
  image: registry.gitlab.com/sbenv/veroxis/images/kaniko:1.23.1
  needs: ["zigbuild:build:x86_64"]
  tags: ["_docker", "x86_64"]
  interruptible: true
  artifacts:
    expire_in: 1 days
    paths: ["image.x86_64.tar"]
  script:
    - mv "cargo-zigbuild/target/x86_64-unknown-linux-musl/release/cargo-zigbuild" "cargo-zigbuild-bin"
    - >
      executor
      --image-fs-extract-retry=42
      --push-retry=42
      --context="${CI_PROJECT_DIR}"
      --dockerfile="${CI_PROJECT_DIR}/Dockerfile"
      --destination="${CI_REGISTRY_IMAGE:latest}"
      --snapshot-mode=redo
      --no-push
      --tar-path="image.x86_64.tar"

image:build:aarch64:
  image: registry.gitlab.com/sbenv/veroxis/images/kaniko:1.23.1
  needs: ["zigbuild:build:aarch64"]
  tags: ["_docker", "aarch64"]
  interruptible: true
  artifacts:
    expire_in: 1 days
    paths: ["image.aarch64.tar"]
  script:
    - mv "cargo-zigbuild/target/aarch64-unknown-linux-musl/release/cargo-zigbuild" "cargo-zigbuild-bin"
    - >
      executor
      --image-fs-extract-retry=42
      --push-retry=42
      --context="${CI_PROJECT_DIR}"
      --dockerfile="${CI_PROJECT_DIR}/Dockerfile"
      --destination="${CI_REGISTRY_IMAGE:latest}"
      --snapshot-mode=redo
      --no-push
      --tar-path="image.aarch64.tar"

image:publish:release:
  image: registry.gitlab.com/sbenv/veroxis/images/podman:5.2.2
  interruptible: true
  only: ["master"]
  needs:
    - image:build:x86_64
    - image:build:aarch64
  script:
    - podman login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
    - podman manifest create "${CI_REGISTRY_IMAGE}:latest"
    - podman manifest add "${CI_REGISTRY_IMAGE}:latest" docker-archive:./image.x86_64.tar
    - podman manifest add "${CI_REGISTRY_IMAGE}:latest" docker-archive:./image.aarch64.tar
    - podman manifest push --all --format="v2s2" "${CI_REGISTRY_IMAGE}:latest" "docker://${CI_REGISTRY_IMAGE}:latest"
    - podman manifest push --all --format="v2s2" "${CI_REGISTRY_IMAGE}:latest" "docker://${CI_REGISTRY_IMAGE}:$(./parse_zigbuild_version.sh)"
